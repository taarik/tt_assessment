<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200301151519 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE movie (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, actors VARCHAR(255) NOT NULL, image_1 VARCHAR(255) NOT NULL, image_2 VARCHAR(255) NOT NULL, year DATE NOT NULL, start_date DATE NOT NULL, end_date DATE NOT NULL, runtime SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE timeslot (id INT AUTO_INCREMENT NOT NULL, day VARCHAR(32) NOT NULL, start_time TIME NOT NULL, end_time TIME NOT NULL, active SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE booking (id INT AUTO_INCREMENT NOT NULL, movie_id INT NOT NULL, customer_id INT DEFAULT NULL, cinema_id INT NOT NULL, theatre_id INT NOT NULL, timeslot_id INT NOT NULL, booking_date DATE NOT NULL, booking_reference VARCHAR(255) NOT NULL, status VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME DEFAULT NULL, INDEX IDX_E00CEDDE8F93B6FC (movie_id), INDEX IDX_E00CEDDE9395C3F3 (customer_id), INDEX IDX_E00CEDDEB4CB84B6 (cinema_id), INDEX IDX_E00CEDDEC80060CD (theatre_id), INDEX IDX_E00CEDDEF920B9E9 (timeslot_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cinema (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE theatre (id INT AUTO_INCREMENT NOT NULL, cinema_id_id INT NOT NULL, name VARCHAR(255) NOT NULL, capacity SMALLINT NOT NULL, active SMALLINT NOT NULL, INDEX IDX_C08D8005F4CB0151 (cinema_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME DEFAULT NULL, last_login DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE8F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDEB4CB84B6 FOREIGN KEY (cinema_id) REFERENCES cinema (id)');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDEC80060CD FOREIGN KEY (theatre_id) REFERENCES theatre (id)');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDEF920B9E9 FOREIGN KEY (timeslot_id) REFERENCES timeslot (id)');
        $this->addSql('ALTER TABLE theatre ADD CONSTRAINT FK_C08D8005F4CB0151 FOREIGN KEY (cinema_id_id) REFERENCES cinema (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE8F93B6FC');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDEF920B9E9');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDEB4CB84B6');
        $this->addSql('ALTER TABLE theatre DROP FOREIGN KEY FK_C08D8005F4CB0151');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDEC80060CD');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE9395C3F3');
        $this->addSql('DROP TABLE movie');
        $this->addSql('DROP TABLE timeslot');
        $this->addSql('DROP TABLE booking');
        $this->addSql('DROP TABLE cinema');
        $this->addSql('DROP TABLE theatre');
        $this->addSql('DROP TABLE customer');
    }
}
