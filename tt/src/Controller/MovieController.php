<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Repository\MovieRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/movie")
 */
class MovieController extends AbstractController
{
	CONST HEADER_CACHE_CONTROL = 3600;

	/**
	 * @var MovieRepository
	 */
	private $movieRepository;
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	public function __construct(
		MovieRepository $movieRepository,
		LoggerInterface $logger
	)
	{
		$this->movieRepository = $movieRepository;
		$this->logger = $logger;
	}


    /**
     * @Route("/", name="movie_index")
     */
    public function index()
    {
    	$movies = $this->movieRepository->findAll();

        $response = $this->render('movie/index.html.twig', ['movies' => $movies]);

	    return $this->setResponseHeaders($response);

    }

	/**
	 * @Route("/view/{id}", name="movie_view")
	 */
    public function view(Movie $movie)
    {
	    $response = new Response($this->render('movie/view.html.twig', ['movie' => $movie]));

		return $this->setResponseHeaders($response);
    }

	/**
	 * convenience method for set response headers + less duplication
	 *
	 * @param Response $response
	 * @return Response
	 */
    protected function setResponseHeaders(Response $response)
    {
	    $response->setSharedMaxAge(static::HEADER_CACHE_CONTROL);

	    $response->headers->addCacheControlDirective('must-revalidate', true);

	    return $response;
    }
}
