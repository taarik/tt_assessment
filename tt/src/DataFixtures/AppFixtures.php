<?php

namespace App\DataFixtures;

use App\Entity\{Booking, Cinema, Customer, Movie, Theatre, Timeslot};
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
	public function load(ObjectManager $manager)
	{
		$salt = 'rando salt. This will be getEnv APP_SECRET once I figure it out';

		$customers = [];
		for ($i=0; $i<5; $i++) {
			$customer = New Customer();
			$customer->setFirstName('Jack' . rand(1,50));
			$customer->setLastName('Jack' . rand(1,50));
			$customer->setEmail('Jack+' . rand(1,50) . '@gmail.com');
			$customer->setPassword(password_hash($salt . rand(1,1000000), PASSWORD_BCRYPT));
			$customer->setCreatedAt(new \DateTime());
			$manager->persist($customer);
			$manager->flush();
			$customers[] = $customer;
		}

		$cinemas = [];
		$theatres = [];
		for ($i=0; $i<2; $i++) {
			$cinema = new Cinema();
			$cinema->setName('Cinema Numero ' . rand(1,1000));
			$cinema->setDescription('A fancy cinema '. rand(1,1000));
			$cinema->setAddress(rand(1,1000). ' Long Street, Cape Town');
			$manager->persist($cinema);
			$manager->flush();
			$cinemas[] = $cinema;

			for ($j=0; $j<2; $j++) {
				$theatre = new Theatre();
				$theatre->setName('Theatre number random');
				$theatre->setCapacity(30);
				$theatre->setCinemaId($cinema);
				$theatre->setActive(1);
				$manager->persist($theatre);
				$manager->flush();
				$theatres[] = $theatre;
			}
		}

		$addTimeslots = [
			[date('Y-m-d 9:00'), date('Y-m-d 11:00')],
			[date('Y-m-d 13:00'),date('Y-m-d 15:00')]
		];

		$timeslots = [];
		foreach ($addTimeslots as $addTimeslot) {
			$timeslot = new Timeslot();
			$timeslot->setDay('Monday');
			$timeslot->setStartTime(new \DateTime($addTimeslot[0]));
			$timeslot->setEndTime(new \DateTime($addTimeslot[1]));
			$timeslot->setActive(1);
			$manager->persist($timeslot);
			$manager->flush();
			$timeslots[] = $timeslot;
		}

		$movies = [];
		for ($i=0; $i<4; $i++) {
			$movie = new Movie();
			$movie->setName('A movie named ' . rand(1,1000));
			$movie->setDescription('Lorem Ipsum is simply dummy text of the printing and typesetting industry.');
			$movie->setActors('Joe Bloggs, Arnold Palmer');
			$movie->setImg1('movie_' . ($i+1) . '_main.jpg');
			$movie->setImg2('movie_' . ($i+1) . '_alt.jpg');
			$movie->setYear(new \DateTime('2020'));
			$movie->setStartDate(new \DateTime('2020-01-01'));
			$movie->setEndDate(new \DateTime('2021-01-01'));
			$movie->setRuntime(120 + $i);
			$manager->persist($movie);
			$manager->flush();
			$movies[] = $movie;
		}

		$bookingStatuses = ['started', 'booked', 'expired', 'cancelled'];
		for ($j=0; $j<10; $j++) {
			$customer = $customers[rand(0, count($customers)-1)];
			$theatre = $theatres[rand(0, count($theatres)-1)];
			$cinema = $cinemas[rand(0, count($cinemas)-1)];
			$movie = $movies[rand(0, count($movies)-1)];
			$timeslot = $timeslots[rand(0, count($timeslots)-1)];

			$bookingReference = date('Ymd') . '-' . $timeslot->getId()
				. '-' . $theatre->getId() . '-' . $customer->getId() . '-' . uniqid();

			$booking = new Booking();
			$booking->setMovie($movie);
			$booking->setCustomer($customer);
			$booking->setCinema($cinema);
			$booking->setTheatre($theatre); // but should be based on cinema parent
			$booking->setTimeslot($timeslot);
			$booking->setBookingDate(new \DateTime(date('Y-m-d')));
			$booking->setBookingReference($bookingReference);
			$booking->setStatus($bookingStatuses[rand(0,count($bookingStatuses)-1)]); // started|booked|expired|cancelled - lock for update throughout process, job runner for expiry
			$booking->setCreatedAt(new \DateTime());
			$manager->persist($booking);
		}

		$manager->flush();
	}
}
