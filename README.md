# Project README

## First install the following dependencies:
- Node.js >= 8 (required for yarn)
- PHP Composer
- yarn
- docker
- Redis (https://stitcher.io/blog/php-74-upgrade-mac)
    - `pecl install redis`
    - `brew install imagemagick`
    - `pecl install imagemagick`
    - `pecl install xdebug`

## Add your own .env file to `docker/.`
```
DATABASE_NAME=tt
DATABASE_USER=tt_app_user
DATABASE_PASSWORD={your app password here}
DATABASE_ROOT_PASSWORD={your own root password here}

APP_ENV=dev
APP_SECRET={your secret here}
```

## Installation
`docker-compose up -d` from within the docker directory

### Data migrations
First, you'll need create a user for the database:
```
grant all on {database_name}.* to '{database_user}'@'%' identified by '{database_password}';
grant all on {database_name}.* to '{database_user}'@'localhost' identified by '{database_password}';
```

To create the database tables & seed data:
- `php bin/console doctrine:database:create`

Create a migration diff:
- `php bin/console doctrine:migrations:diff`
which outputs:
```
Generated new migration class to "/Users/taarik.siers/Workspace/tt_assessment/tt/src/Migrations/{full_version_name_here}.php"
```

To run & revert a migration:
- `php bin/console doctrine:migrations:execute --up {full_version_name_here}`
- `php bin/console doctrine:migrations:execute --down {full_version_name_here}`

- `php bin/console doctrine:migrations:migrate`
- `php bin/console doctrine:fixtures:load`

How to revert migrations?

## Other information:

### Symfony steps pre data migrations:
- `composer create-project symfony/website-skeleton tt`
- `composer require doctrine maker`
- `composer require --dev doctrine/doctrine-fixtures-bundle`
- `composer require symfony/webpack-encore-bundle`
- `yarn install`
- `yarn add @symfony/webpack-epncore —dev`


## TODO

- [x] DB updates: movie main + alternate image, uniq booking reference
- [x] movie controllers for listing and viewing
- [ ] Caching (Doctrine + HTTP)
- [ ] registration/login for users
- [ ] make booking (lock for update)
- [ ] FE menu system
- [x] Where to save image assets in symfony
- [ ] nice to have 
    - [ ] browse cinema/theatre
    - [ ] admin user (edit movies?)

## Performance Enhancements
- [x] [OPCache](https://symfony.com/doc/current/performance.html)

## Pacakages
- `composer require admin`
- `composer require symfony/security-bundle`
